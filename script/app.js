const navMove = () => {
    const burger = document.querySelector('.ham-menu');
    const nav = document.querySelector('.nav-links');

    burger.addEventListener('click', () => {
        nav.classList.toggle('nav-active');
    });
}

navMove();